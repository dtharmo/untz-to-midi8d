## UNTZ Gate sequencer

My first experimentation with DIY Music started in 2014 with buying an [Adafruit UNTZttrument](https://learn.adafruit.com/untztrument-trellis-midi-instrument/overview).

Simple to solder, works like a charm to control some software through USB-Midi with the Leonardo and the given exemples.

7 years later, I ran my way into modular synth and almost stopped to use my computer to make noises and music. Assuming I have accumulated enough electronics skill, time is come to hack the UNTZtrument toy to works with the synth !

---

I wanted to integrate my toy in my DAW as a peripheral device, so I didn't need to much things like complicated clock generators or other controller feature.

The basic UNTZtrument is a 8x8 grid, so I decided to send out to 8 voices. But for a drum sequencer, 8 steps is not so much, involving to get a pagination feature to reach 64 steps per voice.

With the opportunity to build and use [David Haillant's MIDI8d module](https://www.davidhaillant.com/midi8d-8-digital-outputs/) (which is great !), my toy will out through MIDI channel 10 on notes 36 to 43, one by voice.

I started with this short list :

* MIDI IN (DIN 5)
* MIDI OUT (DIN 5)
* A row of 8 blue leds to represent the page currently readed
* A row of 8 yellow leds to indicate which page is show
* 2 buttons to navigate through the pages

&nbsp;

The firmware has some dependencies, which made life easy.

* [Adafruit Trellis](https://github.com/adafruit/Adafruit_Trellis_Library) to communicate through I2C
* [Trellis XY](https://github.com/Gkuetemeyer/Adafruit_Trellis_XY/) to make the grid mapping array-style
* [MIDI for Arduino](https://github.com/FortySevenEffects/arduino_midi_library) to manage MIDI interactions
* [Button](https://github.com/madleech/Button) to manage buttons interactions
* [StackArray](https://playground.arduino.cc/Code/StackArray) for lazy array manipulation

---

![](https://i.imgur.com/MttMrrD.jpg)

![](https://i.imgur.com/xbV3y8b.png)

TODO link to video
