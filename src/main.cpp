#include <Adafruit_Trellis.h>
#include <Adafruit_Trellis_XY.h>
#include <Arduino.h>
#include <Button.h>
#include <MIDI.h>
#include <StackArray.h>

#define MAX_STEPS 64
#define STEPS_BY_PAGE 8
#define VOICES 8
#define TOTAL_PAGES MAX_STEPS / STEPS_BY_PAGE
#define MIN_BYTES 1
#define MAX_BYTES MIN_BYTES << (TOTAL_PAGES - 1)

#define DATA_PIN_1  10  // DS(14)
#define LATCH_PIN_1 11  // SH_CP(12)
#define CLOCK_PIN_1 12  // ST_CP(11)
#define DATA_PIN_2  4   // DS(14)
#define LATCH_PIN_2  6  // ST_CP(11)
#define CLOCK_PIN_2  5  // SH_CP(12)

#define PREV_BTN 2
#define NEXT_BTN 3

#define NUMTRELLIS 4
#define NKEYS (NUMTRELLIS * 16)
#define BRIGHTNESS 1

#define MIDI8D_CHANNEL 10
#define MIDI_LED 13
#define HIGHEST_MIDI_NOTE 43


Adafruit_Trellis matrix0 = Adafruit_Trellis();
Adafruit_Trellis matrix1 = Adafruit_Trellis();
Adafruit_Trellis matrix2 = Adafruit_Trellis();
Adafruit_Trellis matrix3 = Adafruit_Trellis();
Adafruit_TrellisSet trellis =  Adafruit_TrellisSet(&matrix0, &matrix1,
                                                   &matrix2, &matrix3);
Adafruit_Trellis_XY trellisXY = Adafruit_Trellis_XY();
byte xVal;
byte yVal;
byte grid[TOTAL_PAGES][VOICES];
unsigned long prevReadTime = 0L;

byte step = 0;
byte previous_step = 0;
byte seq_step = 0;

byte page = 0;
byte previous_page = 0;
boolean page_register[8];

byte nav_page = 0;
byte navigation = 1;

Button prev_btn(PREV_BTN);
Button next_btn(NEXT_BTN);

StackArray <byte> playedSteps;

boolean play = false;
uint8_t clock_step = 0;
MIDI_CREATE_DEFAULT_INSTANCE();

boolean change_page = false;

boolean editing = false;


void reset_vars() {
    step = 0;
    previous_step = 0;
    seq_step = 0;

    page = 0;
    previous_page = 0;
    page_register[0] = HIGH;
    page_register[1] = LOW;
    page_register[2] = LOW;
    page_register[3] = LOW;
    page_register[4] = LOW;
    page_register[5] = LOW;
    page_register[6] = LOW;
    page_register[7] = LOW;

    nav_page = 0;
    navigation = 1;

    clock_step = 0;
    play = false;
}

// UNTZ TRELLLIS

void prepareTrellis() {
    trellisXY.begin(NKEYS);
    trellisXY.setOffsets(0, 0, 0);
    trellisXY.setOffsets(1, 4, 0);
    trellisXY.setOffsets(2, 0, 4);
    trellisXY.setOffsets(3, 4, 4);
    trellis.begin(0x72, 0x73, 0x70, 0x71);
    for (byte i = 0; i < trellisXY.numKeys; i++) {
        trellis.clrLED(i);
    }
    trellis.setBrightness(BRIGHTNESS);
    trellis.writeDisplay();
}

void setKey(byte x, byte y, byte state) {
    if (editing) {
        for (int i = 0; i < TOTAL_PAGES; i++) {
            bitWrite(grid[i][y], x, state);
        }
    }
    else {
        bitWrite(grid[nav_page][y], x, state);
    }
}

void checkTrellisInteraction() {
    unsigned long t = millis();
    if((t - prevReadTime) >= 20L) {
        if (trellis.readSwitches()) {
            for (byte i = 0; i < trellisXY.numKeys; i++) {
                if (trellis.justPressed(i)) {
                    delay(10);  // debounce
                    xVal = trellisXY.getTrellisX(i);
                    yVal = trellisXY.getTrellisY(i);
                    if (trellis.isLED(i)) setKey(xVal, yVal, LOW);
                    else setKey(xVal, yVal, HIGH);
                }
            }
        }
        prevReadTime = t;
    }
}

bool isDisplayed(int index, byte step) {
    return bitRead(grid[nav_page][index], step) == 1;
}

bool isOn(int index, byte step) {
    return bitRead(grid[page][index], step) == 1;
}

void displayTrellis() {
    for (byte i = 0; i < trellisXY.numKeys; i++) {
        xVal = trellisXY.getTrellisX(i);
        yVal = trellisXY.getTrellisY(i);
        if (isDisplayed(yVal, xVal)) trellis.setLED(i);
        else trellis.clrLED(i);
    }
    trellis.writeDisplay();
}

void moveCursorBar() {
    if (page != nav_page) return;

    for (byte y = 0; y < VOICES; y++) {
        byte p_id = trellisXY.getTrellisId(previous_step, y);
        if (isOn(y, previous_step)) trellis.setLED(p_id);
        else trellis.clrLED(p_id);

        byte c_id = trellisXY.getTrellisId(seq_step, y);
        if (isOn(y, seq_step)) trellis.clrLED(c_id);
        else trellis.setLED(c_id);
    }
    trellis.writeDisplay();
}

// NAVIGATION / PAGINATION

void shiftNavLeds() {
    digitalWrite(LATCH_PIN_1, LOW);
    shiftOut(DATA_PIN_1, CLOCK_PIN_1, MSBFIRST, navigation);
    digitalWrite(LATCH_PIN_1, HIGH);
}

void paginate() {
    page_register[previous_page] = LOW;
    page_register[page] = HIGH;

    digitalWrite(CLOCK_PIN_2, LOW);
    for (int i = 7; i >= 0; i--) {
        digitalWrite(LATCH_PIN_2, LOW);
        digitalWrite(DATA_PIN_2, page_register[i]);
        digitalWrite(LATCH_PIN_2, HIGH);
    }
    digitalWrite(CLOCK_PIN_2, HIGH);

    change_page = false;
}

void navigate() {
    if (prev_btn.released()) {
        editing = false;

        if (navigation == MIN_BYTES) {
            nav_page = TOTAL_PAGES - 1;
            navigation = MAX_BYTES;
        }
        else {
            nav_page --;
            navigation >>= MIN_BYTES;
        }
        shiftNavLeds();
    }

    if (next_btn.released()) {
        editing = false;
        if (navigation == MAX_BYTES) {
            nav_page = 0;
            navigation = MIN_BYTES;
        }
        else {
            nav_page ++;
            navigation <<= MIN_BYTES;
        }
        shiftNavLeds();
    }
}

// STEPS / MIDI OUT

void playStep(byte pitch) {
    playedSteps.push(pitch);
    MIDI.sendNoteOn(pitch, 64, MIDI8D_CHANNEL);
}

void clearSteps() {
    byte pitch;
    while (!playedSteps.isEmpty()) {
        pitch  = playedSteps.pop();
        MIDI.sendNoteOff(pitch, 64, MIDI8D_CHANNEL);
    }
}

void moveStep() {
    step = (step + 1) % MAX_STEPS;
    previous_step = seq_step;
    seq_step = (seq_step + 1) % STEPS_BY_PAGE;

    previous_page = page;
    page = (int)(step / STEPS_BY_PAGE);
    if (page != previous_page) change_page = true;

    clearSteps();
    for (byte y = 0; y < VOICES; y++) {
        if (isOn(y, seq_step)) playStep(HIGHEST_MIDI_NOTE - y);
    }
}

// MIDI IN

void handleStart() {
    play = true;
}

void handleStop() {
    if (play) {
        reset_vars();
        paginate();
    }
}

void handleClock() {
    if (play) {
        clock_step = clock_step % 96;

        if (clock_step % 6 == 0) {
            digitalWrite(MIDI_LED, HIGH);
            moveCursorBar();
            if (change_page) paginate();
            moveStep();
        }
        else digitalWrite(MIDI_LED, LOW);

        clock_step++;
    }
}

// EDIT MODE

void editMode() {
    if (next_btn.pressed() && prev_btn.pressed()) editing = true;

    if (editing) {
        digitalWrite(CLOCK_PIN_1, LOW);
        for (int i = 7; i >= 0; i--) {
            digitalWrite(LATCH_PIN_1, LOW);
            digitalWrite(DATA_PIN_1, HIGH);
            digitalWrite(LATCH_PIN_1, HIGH);
        }
        digitalWrite(CLOCK_PIN_1, HIGH);
    }
}


void setup() {
    pinMode(LATCH_PIN_1, OUTPUT);
    pinMode(CLOCK_PIN_1, OUTPUT);
    pinMode(DATA_PIN_1, OUTPUT);
    pinMode(LATCH_PIN_2, OUTPUT);
    pinMode(CLOCK_PIN_2, OUTPUT);
    pinMode(DATA_PIN_2, OUTPUT);

    prev_btn.begin();
    next_btn.begin();

    MIDI.setHandleClock(handleClock);
    MIDI.setHandleStart(handleStart);
    MIDI.setHandleStop(handleStop);
    MIDI.begin(MIDI_CHANNEL_OMNI);
    MIDI.turnThruOff();
    pinMode(MIDI_LED, OUTPUT);

    prepareTrellis();

    shiftNavLeds();
    paginate();
}


void loop() {
    editMode();

    MIDI.read();

    navigate();

    checkTrellisInteraction();

    displayTrellis();
}
